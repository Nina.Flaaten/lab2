package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

// Fikse det som mangler

public class Fridge implements IFridge {
    
    int max_size = 20;
    ArrayList<FridgeItem> item,
    items = new ArrayList<FridgeItem>();

    public int totalSize() {
        // TODO The fridge has a fixed (final) max size. Returns the total number of items there is space for in the fridge
        return max_size;
    }

    @Override
    public int nItemsInFridge() {
        // TODO Returns the number of items currently in the fridge
        return items.size();
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        // TODO Place a food item in the fridge. Items can only be placed in the fridge if there is space
        if(items.size() >= totalSize())
            return false;
        return items.add(item);
            
    }

    @Override
    public void takeOut(FridgeItem item) {
        // TODO Remove item from fridge
        if(!items.contains(item))
            throw new NoSuchElementException("Item not in fridge.");
        items.remove(item);
        
    }

    @Override
    public void emptyFridge() {
        // TODO Remove all items from the fridge
        items.clear();
      

        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        // Remove all items that have expired from the fridge. Return a list of all expired items.
        ArrayList<FridgeItem> expiredFood = new ArrayList<>();
        for(int i=0; i < nItemsInFridge(); i++) {
            FridgeItem item = items.get(i);
            if(item.hasExpired()) {
                expiredFood.add(item);
            }
        }
        for(FridgeItem expiredItem : expiredFood) {
            items.remove(expiredItem);
        }
        return expiredFood;
    }
}

